import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TarjetaCredito } from 'src/app/models/TarjetaCredito';

@Component({
  selector: 'app-agregar-tarjeta',
  templateUrl: './agregar-tarjeta.component.html',
  styleUrls: ['./agregar-tarjeta.component.css']
})
export class AgregarTarjetaComponent implements OnInit {

  form: FormGroup;
  constructor(private fb: FormBuilder) { 
    this.form = this.fb.group({
      titular:['',Validators.required],
      numeroTarjeta: ['',[Validators.required, Validators.minLength(16),Validators.maxLength(16)]],
      fechaExpiracion: ['',[Validators.required, Validators.minLength(5),Validators.maxLength(5)]],
      clave:['', [Validators.required, Validators.minLength(3),Validators.maxLength(3)]]
    })
  }

crearTarjeta(){
  const TARJETA:TarjetaCredito ={
    titular:this.form.value.titular,
    numeroTarjeta:this.form.value.numeroTarjeta,
    fechaExpiracion:this.form.value.fechaExpiracion,
    clave:this.form.value.clave,
    fechaCreacion: new Date(),
    fechaActualizacion:new Date()
  };
  console.log(TARJETA);
  
}
    
  

  ngOnInit(): void {
  }

}
