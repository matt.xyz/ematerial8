import { Component, OnInit } from '@angular/core';
import { TarjetaCredito } from 'src/app/models/TarjetaCredito';
import { DatosUsuario } from 'src/app/interfaces/tarjeta.interface';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import { TarjetaService } from 'src/app/servicios/tarjeta.service';
import { MatSnackBar } from '@angular/material/snack-bar';



@Component({
  selector: 'app-listado-tarjeta',
  templateUrl: './listado-tarjeta.component.html',
  styleUrls: ['./listado-tarjeta.component.css']
})


export class ListadoTarjetaComponent implements OnInit {

  listUsuarios:DatosUsuario[] = [];
  displayedColumns: string[] = ['titular', 'numeroTarjeta', 'fechaExpiracion', 'accion'];
  dataSource !: MatTableDataSource<any>;


  constructor(private _usuarioService:TarjetaService, private _snackbar:MatSnackBar) { }

  ngOnInit(): void {
    this.cargarUsuario();
  }
  cargarUsuario(){
  this.listUsuarios=this._usuarioService.getUsuario();
  this.dataSource=new MatTableDataSource(this.listUsuarios);
}
eliminarUsuario(index:number){
console.log(index);
this._usuarioService.eliminarUsuario(index);
this.cargarUsuario();
this._snackbar.open('El Usuario fue eliminado con exito','',{
  duration:1500,
  horizontalPosition:'center',
  verticalPosition:'bottom'
})
}
}
