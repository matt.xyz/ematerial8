export interface DatosUsuario {
  titular: string;
  numeroTarjeta: string;
  fechaExpiracion: string;
}
